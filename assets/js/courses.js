//gets a string
let adminUser = localStorage.getItem('isAdmin');

let addButton = document.querySelector('#adminButton')
let cardFooter // will allow us to add a button at the bottom of each of our courses which can be redirect to the specific course.
let token = localStorage.getItem('token')

let subHeading = document.querySelector('#subHeading')
let pickCourse = document.querySelector('#pickCourse')

if(adminUser === "true"){

	addButton.innerHTML += `
		<div class="col-md-2 offset-md-10 mb-3">
		<a href="./addCourse.html" class="btn btn-primary">Add Course</a>`

	subHeading.innerHTML = `Hi Admin!`
	pickCourse.innerHTML = `Complete Courses List`
	
	} else {

		addButton.innerHTML = null

		subHeading.innerHTML = `Start Learning Today`
		pickCourse.innerHTML = `Pick Your Course`

	}



if(adminUser === "true"){

fetch('https://thawing-hamlet-53628.herokuapp.com/api/courses/view-full-list', {

	headers: {
				"Authorization": `Bearer ${token}`
			}

})


.then(res => res.json())
.then(data => {


	let courseData;

	if(data.length < 1){

		courseData = "No Courses Available"

	} else {

		courseData = data.map(course => {


			if(course.isActive === true){

			cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Courses</a>`

			cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>`

			} else {

			cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Courses</a>`

			cardFooter += `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>`

			}
/*			if(course.isActive === "true") {
				
				cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>`

			} else if(course.isActive === "false") {		
									
				cardFooter += `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>`

			}*/

			return (

				`
					<div class="col-md-4">
						<div class="card course-card">
							<img src="../assets/images/course-1.jpg" class="card-img-top" alt="...">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">Price: ${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>

				`

				)

		}).join("")

		

	}


	
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData

})

} else {


	fetch('https://thawing-hamlet-53628.herokuapp.com/api/courses/')


.then(res => res.json())
.then(data => {


	let courseData;

	if(data.length < 1){

		courseData = "No Courses Available"

	} else {

		courseData = data.map(course => {


			if(adminUser == "false" || !adminUser){

				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Courses</a>`

			}

			return (

				`
					<div class="col-md-4">
						<div class="card course-card">
							<img src="../assets/images/course-1.jpg" class="card-img-top" alt="...">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>

				`

				)

		}).join("")

		

	}


	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData

})


}



