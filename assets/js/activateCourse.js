let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

let token = localStorage.getItem("token")


fetch(`https://thawing-hamlet-53628.herokuapp.com/api/courses/activate/${courseId}`, {

	method: "PUT",
	headers: {

		"Authorization": `Bearer ${token}`

	}

})
.then(res => res.json())
.then(data => {

	if(data){

		alert("Course Activated.")
	} else {
		alert("Something Went Wrong.")
	}

})
