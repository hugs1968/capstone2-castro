let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e)=>{

	e.preventDefault() //prevents the reloading of the page

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	let token = localStorage.getItem("token")



	fetch('https://thawing-hamlet-53628.herokuapp.com/api/courses/', {

		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` //we are sending a token through http. That is to say that we are giving authorization to the bearer of this token.

			//req.headers.authorization
		},

		body: JSON.stringify({
				name: courseName,
				description: description,
				price: price

			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				if (data){

						window.location.replace("./courses.html")

							

					} else {

						alert("Something went wrong. Course not added")

					}

				})

})