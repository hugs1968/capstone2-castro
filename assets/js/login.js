let loginForm = document.querySelector("#logInUser")


loginForm.addEventListener("submit", (e)=>{

	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email === "" || password === ""){

		alert("Please input your email/password.")


		

	} else {

		fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/login', {

			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})		

		})
		.then(res => res.json())
		.then(data => {

			

			if(data !== false && data.accessToken !== null){
				
				localStorage.setItem('token', data.accessToken)

				fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/details', {

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}


				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('id', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)
					localStorage.setItem('courses', JSON.stringify(data.enrollments))
					
					window.location.replace('../index.html')

				})

			}else {

				alert('Login Failed. Something went wrong.')

			}
			

		})

	}

})




let registerForm = document.querySelector("#registerForm")

// .addEventListener("event_name", callbackfunction())
registerForm.addEventListener("submit", (e) => {

	//prevent default disallows the refresh of your page when submitting.
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail1").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	console.log(firstName);
	console.log(lastName);
	console.log(mobileNumber);
	console.log(email);
	console.log(password1);
	console.log(password2);

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		/*
			fetch is a built in js function that allows us to get data from another source without the need to refresh the page.
			This allows to get a response if an email we are trying to register has already been registered in our database.

			fetch sends the data to the url provided with its parameters:

			fetch(<url>,<parameters>)

			parameters may contain:
			//method -> http method (should reflect the http method as defined in your backend)
			//headers 
				-> Content-Type - defines what kind of data to send.
				-> authorization - contains our Bearer Token

			//
			//body -> the body of our requests or req.body

		
		*/




		fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/email-exists', {

			method: 'POST',
			headers: {								//headers from POSTMAN
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
				// email="arvin@gmail.com";firstName="arvin"
			})
			
		})
		.then(res => res.json())
		.then(data => {

			//data => true or false
			//if true, then the email already exists in our database.
			//if false, then the email has yet to be registered.
			//this will check if the email exists or not, if the email exists, then will register the user, if not we're going to show an alert.

			if(data === false){

				//nest a fetch request using the registration to register our user, if the email being registered does not already exists in our database.
				fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/', {

					method: "POST",
					headers: {

						"Content-Type": "application/json"

					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber

					})

				})
				.then(res => res.json())
				.then(data => {
					//in our registration, true is sent as a response for a successful registration, false if registration has failed
					if (data === true){

						alert("Registration Successful")
						//this method allows to replace the current document with the document provided in the method which means that for a successful registration we will be redirected to our login page.
						window.location.replace("./login.html")
						

					} else {

						alert("Registration Failed")

					}

				})

			} else {

				alert("Email Already Exists.")

			}

		})

	} else {

		alert('Invalid Input.')

	}


})