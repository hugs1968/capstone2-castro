let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

let isAdmin = localStorage.getItem('isAdmin')


let token = localStorage.getItem('token')


let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDesc')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')

let firstName = localStorage.getItem("firstName")





if(isAdmin === "true"){

	subHeading.innerHTML = `Hi Admin!`
	pickCourse.innerHTML = `Enrollees List`

	fetch(`https://thawing-hamlet-53628.herokuapp.com/api/courses/${courseId}`, {

		headers: {
				"Authorization": `Bearer ${token}`
			}
	})

.then(res => res.json())
.then(data => {

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price


	let enrolleesList;

	if(data.enrollees.length < 1) {

		enrolleesList = "No Students Enrolled"
	} else {

		enrolleesList = data.enrollees.map(enrollees => {

			
			fetch(`https://thawing-hamlet-53628.herokuapp.com/api/users/${enrollees.userId}`, {

				headers: {

					'Authorization': `Bearer ${token}`
				}

			})
			.then(res => res.json())
			.then(data => {

				data.firstName = data.firstName;
				data.lastName= data.lastName;

				console.log(data)
			})

			return (
			`
				<div class="col-12">
					<div class="card mb-2">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">	
									<h5 class="card-title">${enrollees._id}</h5>
								</div>
								<div class="col-md-4">	
									<p class="card-text">Date Enrolled: ${enrollees.enrolledOn}</p>
								</div>								
							</div>							
						</div>							
					</div>
				</div>

			`
			)

		}).join("")
		
			
	

	}

	let container = document.querySelector("#course-jumbo")

	container.innerHTML += enrolleesList

})





} else if (!token === false) {



fetch(`https://thawing-hamlet-53628.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data=> {


/*	`<div class="row">
		<div class="col-md-4">
		Name:
		</div>
		<div class="col-md-8">
		courseName.innerHTML =	data.name 
		</div>
	 </div>`*/
	courseName.innerHTML =	data.name 
	courseDesc.innerHTML =	data.description
	coursePrice.innerHTML =	data.price



	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

	
	document.querySelector("#enrollButton").addEventListener("click", ()=>{

		

		fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/enroll', {

			method: 'POST',
			headers: {

				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`

			},
			body: JSON.stringify({

				courseId: courseId

			})

		})
		.then(res=> res.json())
		.then(data => {

			if(data){

				alert("You have enrolled successfully.")
				window.location.replace("./courses.html")
			} else {

				alert("Enrollment Failed.")

			}

		})

	})

})





} else if (!token === true) {

	fetch(`https://thawing-hamlet-53628.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data=> {

	courseName.innerHTML =	data.name
	courseDesc.innerHTML =	data.description
	coursePrice.innerHTML =	data.price

	enrollContainer.innerHTML = `Sign Up Now To Enroll<a href="../index.html" <button href="../index.html" class="btn btn-block btn-primary">Sign Up</button></a>`
})

}