let navItems = document.querySelector("#navSession");
let userGreeting = document.querySelector("#userGreeting");

let registerSection = document.querySelector("#registerSection");

let userToken = localStorage.getItem("token");
if(!userToken) {
	navItems.innerHTML +=
	`
				<li class="nav-item">
					<a href="./pages/login.html" class="nav-link"> Log in </a>
				</li>
	`
	
} else {

	navItems.innerHTML += //added profile tab
	`
				<li class="nav-item ">
					<a href="./pages/profile.html" class="nav-link"> Profile </a>
				</li>

				<li class="nav-item">
					<a href="./pages/logout.html" class="nav-link"> Log Out </a>
				</li>

	`
}


if(!userToken) {
	registerSection.innerHTML +=
	`
				<div class="container">
		<div class="row">
			<div class="col-md-7"></div>
			<div class="col-md-5 order-md-last">
				<div class="login-wrap p-4 p-md-5">
					<h3 class="mb-4">Register Now</h3>
					<form id="registerForm" action="#" class="signup-form">
							<div class="form-group">
								<label class="label" for="name">First Name</label>
								<input type="text" id="firstName" class="form-control">
							</div>
							<div class="form-group">
								<label class="label" for="name">Last Name</label>
								<input type="text" id="lastName" class="form-control">
							</div>
							<div class="form-group">
								<label class="label" for="name">Mobile Number</label>
								<input type="number" id="mobileNumber" class="form-control">
							</div>
							<div class="form-group">
								<label class="label" for="email">Email Address</label>
								<input type="text" id="userEmail1" class="form-control">
							</div>
							<div class="form-group">
								<label class="label" for="password">Confirm Password</label>
								<input id="password1" type="password" class="form-control">
							</div>
							<div class="form-group">
								<label class="label" for="password">Confirm Password</label>
								<input id="password2" type="password" class="form-control">
							</div>
							<div class="form-group d-flex justify-content-end mt-4">
								<button type="submit" class="btn btn-primary submit">
									<span class="fa fa-paper-plane">
									</span>
								</button>
							</div>								
					</form>
					<p class="text-center">Already have an account??
						<a href="#signin">Sign In</a>
					</p>
				</div>
			</div>
		</div>

	</div>
	`
	
} else {

	registerSection.innerHTML = null

}




let registerForm = document.querySelector("#registerForm")

// .addEventListener("event_name", callbackfunction())
registerForm.addEventListener("submit", (e) => {

	//prevent default disallows the refresh of your page when submitting.
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail1").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	console.log(firstName);
	console.log(lastName);
	console.log(mobileNumber);
	console.log(email);
	console.log(password1);
	console.log(password2);

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		/*
			fetch is a built in js function that allows us to get data from another source without the need to refresh the page.
			This allows to get a response if an email we are trying to register has already been registered in our database.

			fetch sends the data to the url provided with its parameters:

			fetch(<url>,<parameters>)

			parameters may contain:
			//method -> http method (should reflect the http method as defined in your backend)
			//headers 
				-> Content-Type - defines what kind of data to send.
				-> authorization - contains our Bearer Token

			//
			//body -> the body of our requests or req.body

		
		*/




		fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/email-exists', {

			method: 'POST',
			headers: {								//headers from POSTMAN
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
				// email="arvin@gmail.com";firstName="arvin"
			})
			
		})
		.then(res => res.json())
		.then(data => {

			//data => true or false
			//if true, then the email already exists in our database.
			//if false, then the email has yet to be registered.
			//this will check if the email exists or not, if the email exists, then will register the user, if not we're going to show an alert.

			if(data === false){

				//nest a fetch request using the registration to register our user, if the email being registered does not already exists in our database.
				fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/', {

					method: "POST",
					headers: {

						"Content-Type": "application/json"

					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber

					})

				})
				.then(res => res.json())
				.then(data => {
					//in our registration, true is sent as a response for a successful registration, false if registration has failed
					if (data === true){

						alert("Registration Successful")
						//this method allows to replace the current document with the document provided in the method which means that for a successful registration we will be redirected to our login page.
						window.location.replace("./pages/login.html")
						

					} else {

						alert("Registration Failed")

					}

				})

			} else {

				alert("Email Already Exists.")

			}

		})

	} else {

		alert('Invalid Input.')

	}


})




/*let isAdmin = localStorage.getItem('isAdmin')
console.log(isAdmin)
//data type? getting items from the localstorage, they are strings.

if(!userToken) {

	userGreeting.innerHTML += `	Hello, Guest!`	
} else {

	if(isAdmin === "true"){

		userGreeting.innerHTML += `	Hello, Admin!`
	
	} else {

		userGreeting.innerHTML += `	Hello, User!`	

}

}*/

