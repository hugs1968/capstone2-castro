let profileContainer = document.querySelector("#profileContainer")
let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem('token')

let profileDetails = document.querySelector("#profile-jumbo")

let subHeading = document.querySelector('#subHeading')
let pickCourse = document.querySelector('#pickCourse')




if(adminUser === "true" || !token === true){

	profileContainer.innerHTML = `<img id="errorSvg" src="../assets/images/error.svg" class="image" alt="" />`
	subHeading.innerHTML = `Sorry!`
	pickCourse.innerHTML = `Error 404! Page Not Found!`
} else {

fetch('https://thawing-hamlet-53628.herokuapp.com/api/users/details', {

		headers: {

			Authorization: `Bearer ${token}`

		}


	})
	.then(res => res.json())
	.then(data => {

		console.log(data)
		profileDetails.innerHTML = `

				<h2>Student Name: ${data.firstName} ${data.lastName}</h2>

				<p>Mobile Number: ${data.mobileNo}</p>


				`



		let userData;

		if(data.enrollments.length < 1){

		userData = "No Courses Enrolled"

		} else {

		userData = data.enrollments.map(user => {


			return (

				`
					

					<div class="col-md-12">
						<div class="card mb-2">
							<div class="card-body">
								<div class="row">
									<div class="col-md-4">	
										<h5 class="card-title">Course Name: ${user._id}</h5>
									</div>
									<div class="col-md-4">	
										<p class="card-text">Date Enrolled: ${user.enrolledOn}</p>
									</div>
									<div class="col-md-4">	
										<p class="card-text">Status: ${user.status}</p>
									</div>								
								</div>
								
							</div>							
						</div>
					</div>

				`

				)

		}).join("")

			

		}


		let container = document.querySelector("#profile-jumbo")

		container.innerHTML += userData



})	

}
