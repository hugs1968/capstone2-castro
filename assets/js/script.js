let navItems = document.querySelector("#navSession");

let userToken = localStorage.getItem("token");

if(!userToken) {


	navItems.innerHTML +=
	`
				<li class="nav-item ">
					<a href="./login.html" class="nav-link"> Log in  /  Register </a>
				</li>

	`
} else {

	navItems.innerHTML += //added profile tab
	`
				<li class="nav-item ">
					<a href="./profile.html" class="nav-link"> Profile </a>
				</li>

				<li class="nav-item ">
					<a href="./logout.html" class="nav-link"> Log Out </a>
				</li>
				

	`

}